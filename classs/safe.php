<?php
	class safe
	{
		//过滤javascript,css,iframes,object等不安全参数
		function fliter_script($value) 
		{
			$value=preg_replace("/(javascript:)?on(click|load|key|mouse|error|abort|move|unload|change|dblclick|move|reset|resize|submit)/i","&111n\\2",$value);
			$value=preg_replace("/(.*?)<\/script>/si","",$value);
			$value=preg_replace("/(.*?)<\/iframe>/si","",$value);
			$value=preg_replace ("//iesU",'',$value);
			return $value;
		}
		//过滤HTML标签
		function fliter_html($value) 
		{
			if(function_exists('htmlspecialchars')) 
			{
				return htmlspecialchars($value);
			}
			return str_replace(array("&",'"',"'","<",">"),array("&","\"","'","<",">"),$value);
		}
		//过滤sql
		function fliter_sql($value) 
		{
			$sql=array("select",'insert',"update","delete","\'","\/\*","\.\.\/","\.\/","union","into","load_file","outfile");
			$sql_re=array("","","","","","","","","","","","");
			return str_replace($sql,$sql_re,$value);
		}
		//通用过滤
		function fliter_escape($value) 
		{
			if(is_array($value)) 
			{
				foreach($value as $k=>$v) 
				{
					$value[$k]=self::fliter_str($value[$k]);
					$value[$k]=self::fliter_html($value[$k]);
					$value[$k]=self::fliter_sql($value[$k]);
					
				}
			} 
			else 
			{
				$value=self::fliter_str($value);
				$value=self::fliter_html($value);
				$value=self::fliter_sql($value);
			}
			return $value;
		}
		//过滤字符
		function fliter_str($value) 
		{
			$badstr=array("\0","%00","\r",'&',' ','"',"'","<",">","   ","%3C","%3E");
			$newstr=array('','','','&',' ','"',"''","<",">","   ","<",">");
			$value=str_replace($badstr,$newstr,$value);
			$value=preg_replace('/&((#(\d{3,5}|x[a-fA-F0-9]{4}));)/','&\\1',$value);
			return $value;
		}
		//路径安全转化
		function filter_dir($fileName) 
		{
			$tmpname=strtolower($fileName);
			$temp=array(':/',"\0", "..");
			if(str_replace($temp,'',$tmpname)!==$tmpname)
			{
				return false;
			}
			return $fileName;
		}
		//过滤目录
		public function filter_path($path) 
		{
			$path=str_replace(array("'",'#','=','`','$','%','&',';'),'',$path);
			return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/','/',$path),'/');
		}
		//过滤PHP标签
		public function filter_phptag($string) 
		{
			return str_replace(array(''),array('<?','?>'),$string);
		}
		//返回函数
		function str_out($value) {
			$badstr=array("<",">","%3C","%3E");
			$newstr=array("<",">","<",">");
			$value=str_replace($newstr,$badstr,$value);
			return stripslashes($value);
		}
	}
?>