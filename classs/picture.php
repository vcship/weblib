<?php
class picture{
	//图片保存的路径 
	private $path;
	
	//实例图像对象时传递图像的一个路径，默认值是当前目录
	function __construct($path="./uploads/"){
		$this->path=rtrim($path,"/")."/";
	}
	
	//设置路径
	function setPath($path){
		$this->path=$path;
	}
	
	//对指定的图像进行缩放
	function thumb($name,$width,$height,$prefix="thumb_"){
		$imgInfo=$this->getInfo($name);
		$srcImg=$this->getImg($name,$imgInfo);
		$size=$this->getNewSize($name,$width,$height,$imgInfo);
		$newImg=$this->kidOfImage($srcImg,$size,$imgInfo);
		return $this->createNewImage($newImg,$prefix.$name,$imgInfo);
	}

	//为图片添加水印
	function waterMark($groundName,$waterName,$waterPos=0,$prefix="water_"){
		$curpath=rtrim($this->path,"/")."/";
		$dir=dirname($waterName);
		if($dir=="."){
			$wpath=$curpath;
		}
		else{
			$wpath=$dir."/";
			$waterName=basename($waterName);
		}
		if(file_exists($curpath.$groundName)&&file_exists($wpath.$waterName)){
			$groundInfo=$this->getInfo($groundName);        
			$waterInfo=$this->getInfo($waterName,$dir);      
			if(!$pos=$this->position($groundInfo,$waterInfo,$waterPos)){
				echo '水印不应该比背景图片小！';
				return false;
			}
			$groundImg = $this->getImg($groundName, $groundInfo);  
			$waterImg = $this->getImg($waterName, $waterInfo, $dir); 
			$groundImg = $this->copyImage($groundImg, $waterImg, $pos, $waterInfo);
			return $this->createNewImage($groundImg, $prefix.$groundName, $groundInfo);
		}
		else{
			echo '图片或水印图片不存在！';
			return false;
		}
	}

	//在一个大的背景图片中剪裁出指定区域的图片
	function cut($name,$x,$y,$width,$height,$prefix="cut_"){
		$imgInfo=$this->getInfo($name);         
		if((($x+$width)>$imgInfo['width'])||(($y+$height)>$imgInfo['height'])){
			echo "裁剪的位置超出了背景图片范围!";
			return false;
		}
		$back=$this->getImg($name,$imgInfo);     
		$cutimg=imagecreatetruecolor($width,$height);
		imagecopyresampled($cutimg,$back,0,0,$x,$y,$width,$height,$width,$height);
		imagedestroy($back);
		return $this->createNewImage($cutimg,$prefix.$name,$imgInfo);
	}

	//用来确定水印图片的位置 
	private function position($groundInfo,$waterInfo,$waterPos){
		if(($groundInfo["width"]<$waterInfo["width"])||($groundInfo["height"]<$waterInfo["height"])){
			return false;
		}
		switch($waterPos) {
			case 1:     //1为顶端居左
				$posX=0;
				$posY=0;
				break;
			case 2:     //2为顶端居中
				$posX=($groundInfo["width"] - $waterInfo["width"])/2;
				$posY=0;
				break;
			case 3:     //3为顶端居右
				$posX=$groundInfo["width"]-$waterInfo["width"];
				$posY=0;
				break;
			case 4:     //4为中部居左
				$posX=0;
				$posY=($groundInfo["height"]-$waterInfo["height"])/2;
				break;
			case 5:     //5为中部居中
				$posX=($groundInfo["width"]-$waterInfo["width"])/2;
				$posY=($groundInfo["height"]-$waterInfo["height"])/2;
				break;
			case 6:     //6为中部居右
				$posX=$groundInfo["width"]-$waterInfo["width"];
				$posY=($groundInfo["height"]-$waterInfo["height"])/2;
				break;
			case 7:     //7为底端居左
				$posX=0;
				$posY=$groundInfo["height"]-$waterInfo["height"];
				break;
			case 8:     //8为底端居中
				$posX=($groundInfo["width"]-$waterInfo["width"])/2;
				$posY=$groundInfo["height"]-$waterInfo["height"];
				break;
			case 9:     //9为底端居右
				$posX=$groundInfo["width"]-$waterInfo["width"];
				$posY=$groundInfo["height"]-$waterInfo["height"];
				break;
			case 0:
			default:    //随机
				$posX=rand(0,($groundInfo["width"]-$waterInfo["width"]));
				$posY=rand(0,($groundInfo["height"]-$waterInfo["height"]));
				break;
		}
		return array("posX"=>$posX,"posY"=>$posY);
	}

	//用于获取图片的属性信息（宽度、高度和类型） 
	private function getInfo($name,$path="."){
		$spath=$path=="."?rtrim($this->path,"/")."/":$path.'/';
		$data=getimagesize($spath.$name);
		$imgInfo["width"]=$data[0];
		$imgInfo["height"]=$data[1];
		$imgInfo["type"]=$data[2];
		return $imgInfo;
	}

	//用于创建支持各种图片格式（jpg,gif,png三种）资源 
	private function getImg($name,$imgInfo,$path='.'){
		$spath=$path=="."?rtrim($this->path,"/")."/":$path.'/';
		$srcPic=$spath.$name;
		switch($imgInfo["type"]){
			case 1:         //gif
				$img=imagecreatefromgif($srcPic);
				break;
			case 2:         //jpg
				$img=imagecreatefromjpeg($srcPic);
				break;
			case 3:         //png
				$img=imagecreatefrompng($srcPic);
				break;
			default:
				return false;
				break;
		}
		return $img;
	}

	//返回等比例缩放的图片宽度和高度，如果原图比缩放后的还小保持不变
	private function getNewSize($name,$width,$height,$imgInfo){
		$size["width"]=$imgInfo["width"];     
		$size["height"]=$imgInfo["height"];   
		if($width<$imgInfo["width"]){
			$size["width"]=$width;          
		}
		if($height<$imgInfo["height"]){
			$size["height"]=$height;        
		}
		if($imgInfo["width"]*$size["width"]>$imgInfo["height"]*$size["height"]){
			$size["height"]=round($imgInfo["height"]*$size["width"]/$imgInfo["width"]);
		}
		else{
			$size["width"]=round($imgInfo["width"]*$size["height"]/$imgInfo["height"]);
		}
		return $size;
	}

	//用于保存图像，并保留原有图片格式 
	private function createNewImage($newImg,$newName,$imgInfo){
		$this->path=rtrim($this->path,"/")."/";
		switch($imgInfo["type"]){
			case 1:       //gif
				$result=imageGIF($newImg, $this->path.$newName);
				break;
			case 2:       //jpg
				$result=imageJPEG($newImg,$this->path.$newName);
				break;
			case 3:       //png
				$result=imagePng($newImg, $this->path.$newName);
				break;
		}
		imagedestroy($newImg);
		return $newName;
	}

	//用于加水印时复制图像 
	private function copyImage($groundImg,$waterImg,$pos,$waterInfo){
		imagecopy($groundImg,$waterImg,$pos["posX"],$pos["posY"],0,0,$waterInfo["width"],$waterInfo["height"]);
		imagedestroy($waterImg);
		return $groundImg;
	}

	//处理带有透明度的图片保持原样 
	private function kidOfImage($srcImg,$size,$imgInfo){
		$newImg=imagecreatetruecolor($size["width"],$size["height"]);
		$otsc=imagecolortransparent($srcImg);
		if($otsc>=0&&$otsc<imagecolorstotal($srcImg)){
			$transparentcolor=imagecolorsforindex($srcImg,$otsc);
			$newtransparentcolor=imagecolorallocate(
				$newImg,
				$transparentcolor['red'],
				$transparentcolor['green'],
				$transparentcolor['blue']
			);
			imagefill($newImg,0,0,$newtransparentcolor);
			imagecolortransparent($newImg,$newtransparentcolor);
		}
		imagecopyresized($newImg,$srcImg,0,0,0,0,$size["width"],$size["height"],$imgInfo["width"],$imgInfo["height"]);
		imagedestroy($srcImg);
		return $newImg;
	}
}