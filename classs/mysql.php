<?php
	class mysql
	{
		private $Host;
		private $dbName;
		private $UserName;
		private $Password;
		private $dbCharSet='utf8';
		private $errorMsg=null;
		private $lastSql=null;
		private $linkID=null;
		private $queryID=null;
		//连接数据库
		public function connect($host,$dbname,$username,$password)
		{
			$this->Host=$host;
            $this->dbName=$dbname;
			$this->UserName=$username;
			$this->Password=$password;
			if($this->linkID==null)
			{
				$this->linkID=mysql_connect($this->Host,$this->UserName,$this->Password);
			}
			if(!$this->linkID)
			{
				$this->errorMsg='数据库连接错误:'.mysql_error();
				return false;
			}
			if(!mysql_select_db($this->dbName,$this->linkID)) 
			{
				$this->errorMsg='打开数据库失败:'.mysql_error($this->linkID);
				return false;
			}
			mysql_query("SET NAMES '".$this->dbCharSet."'",$this->linkID);
			return true;
		}
		//执行sql语句
		public function execute($sql) 
		{
			if($this->linkID!=null||$this->linkID!=false)
			{
				$this->lastSql=$sql;
				$this->queryID=mysql_query($sql);
				if($this->queryID==false) 
				{
					$this->errorMsg='SQL语句执行失败:'.mysql_error($this->linkID);
					return false;
				} 
				else 
				{
					return true;
				}
			}
			else
			{
				$this->errorMsg='数据库连接错误:'.mysql_error();
				return false;
			}
		}
		//获取最后一次连接ID
		public function getLinkID()
		{
			return $this->linkID;
		}
		//获取最后一次查询ID
		public function getQueryId()
		{
			return $this->queryID;
		}
		//获取最后一次数据库操作错误信息
		public function getLastError() 
		{
			return $this->errorMsg;
		}
		//获取最后一次执行的SQL语句
		public function getLastSql() 
		{
			return $this->lastSql;
		}
		//获取最后一次插入数据库记录的索引ID号
		public function getLastInsID() 
		{
			if($this->queryID!=null&&$this->queryID!=false)
			{
				return mysql_num_rows($this->queryID);
			}
			return flase;
		}
		//获取上一次操作影响的行数
		public function getAffectedRows() 
		{
			if($this->linkID!=null&&$this->linkID!=false)
			{
				return mysql_affected_rows($this->linkID);
			}
			false;
		}
		//获取记录集的行数
		public function getRowsNum() 
		{
			if($this->queryID!=null&&$this->queryID!=false)
			{
				return mysql_num_rows($this->queryID);
			}
			return flase;
		}
		//获取记录集
		public function getRows()
		{
			if($this->queryID!=null&&$this->queryID!=false)
			{
				$result=array();
				while($row=mysql_fetch_array($this->queryID)) 
				{
					$result[]=$row;
				}
				return $result;
			}
			return false;
		}
		//获取记录
		public function getRow(){
			if($this->queryID!=null&&$this->queryID!=false)
			{
				while($row=mysql_fetch_array($this->queryID)) 
				{
					return $row;
				}
			}
			return false;
		}
		//释放资源
		public function free() 
		{
			if($this->queryID!=null)
			{
				mysql_free_result($this->queryID);
			}
			$this->queryID=null;
		}
		//关闭连接
		public function close()
		{
			mysql_close($this->linkID);
		}
	}
?>