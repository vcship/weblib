<?php
	class common{
		//格式化时间
		public function secondFormat($second_time){
			$time=intval($second_time)+"秒";  
			if(intval($second_time)>60){
				$second=intval($second_time)%60;  
				$min=intval($second_time/60);  
				$time=$min."分".$second."秒";  
				if($min>60){  
					$min=intval($second_time/60)%60;  
					$hour=intval(intval($second_time/60)/60);  
					$time=$hour."小时".$min."分".$second."秒";  
					if($hour>24){  
						$hour=intval(intval($second_time/60)/60)%24;  
						$day=intval(intval(intval($second_time/60)/60)/24);  
						$time=$day."天".$hour."小时".$min."分".$second."秒";  
					}  
				}  
			}  
			return $time;          
		}
		//是否是移动端
		function isMobile()
		{ 
			if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
			{
				return true;
			} 
			if (isset ($_SERVER['HTTP_VIA']))
			{ 
				return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
			} 
			if (isset ($_SERVER['HTTP_USER_AGENT']))
			{
				$clientkeywords = array ('nokia',
					'sony',
					'ericsson',
					'mot',
					'samsung',
					'htc',
					'sgh',
					'lg',
					'sharp',
					'sie-',
					'philips',
					'panasonic',
					'alcatel',
					'lenovo',
					'iphone',
					'ipod',
					'blackberry',
					'meizu',
					'android',
					'netfront',
					'symbian',
					'ucweb',
					'windowsce',
					'palm',
					'operamini',
					'operamobi',
					'openwave',
					'nexusone',
					'cldc',
					'midp',
					'wap',
					'mobile'
					); 
				if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
				{
					return true;
				} 
			} 
			if (isset ($_SERVER['HTTP_ACCEPT']))
			{ 
				if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
				{
					return true;
				} 
			} 
			return false;
		}
		//生成图片验证码
		public function getCode($num,$w,$h,$name){
			if(!function_exists('gd_info')){
				return false;
			}
			if(get_config('session')!='on')
			{
				session_start();
			}
			$code=""; 
			for($i=0;$i<$num;$i++) 
			{ 
				$code.=rand(0,9); 
			} 
			session($name,$code);  
			header("Content-type: image/PNG"); 
			$im=imagecreate($w,$h); 
			$black=imagecolorallocate($im,0,0,0); 
			$gray=imagecolorallocate($im,200,200,200); 
			$bgcolor=imagecolorallocate($im,255,255,255); 
			imagefill($im,0,0,$gray); 
			imagerectangle($im,0,0,$w-1,$h-1,$black);  
			$style=array($black,$black,$black,$black,$black, 
				$gray,$gray,$gray,$gray,$gray 
			); 
			imagesetstyle($im,$style); 
			$y1=rand(0,$h); 
			$y2=rand(0,$h); 
			$y3=rand(0,$h); 
			$y4=rand(0,$h); 
			imageline($im,0,$y1,$w,$y3,IMG_COLOR_STYLED); 
			imageline($im,0,$y2,$w,$y4,IMG_COLOR_STYLED);  
			for($i=0;$i<80;$i++) { 
				imagesetpixel($im,rand(0,$w),rand(0,$h),$black); 
			} 
			$strx=rand(3,8); 
			for($i=0;$i<$num;$i++){ 
				$strpos=rand(1,6); 
				imagestring($im,5,$strx,$strpos,substr($code,$i,1),$black); 
				$strx+=rand(8,12); 
			} 
			imagepng($im);
			imagedestroy($im);
			return true;
		}
		//保存上传文件
		public function fileupload($name,$path,$size=6144,$type=array('image/jpeg','image/png'))
		{
			if(isset($_FILES[$name])&&is_dir($path)){
				$fname=$_FILES[$name]['name'];
				$ftmp_name=$_FILES[$name]['tmp_name'];
				$fsize=$_FILES[$name]['size'];
				$ferror=$_FILES[$name]['error'];
				$ftype=$_FILES[$name]["type"];
				if(is_array($fname))
				{
					$check=1;
					for($i=0;$i<count($fname);$i++)
					{
						if(!in_array($ftype[$i],$type)||$fsize[$i]>$size||$ferror[$i]>0)
						{
							$check=0;
						}
					}
					if($check==1)
					{
						for($i=0;$i<count($fname);$i++)
						{
							$aryStr=explode(".",$fname[$i]);
							$file=$path.date('YmdHis')."_".uniqid().'.'.strtolower($aryStr[count($aryStr)-1]);
							move_uploaded_file($ftmp_name[$i],$file);
							$filearr[]=$file;
						}
						for($i=0;$i<count($filearr);$i++)
						{
							$filearr[$i]=str_replace($_SERVER['DOCUMENT_ROOT'],'',$filearr[$i]);
						}
						return $filearr;
					}
				}
				else
				{
					if(in_array($ftype,$type)||$fsize<$size)
					{
						$aryStr=explode(".",$fname);
						$file=$path.date('YmdHis')."_".uniqid().'.'.strtolower($aryStr[count($aryStr)-1]);
						move_uploaded_file($ftmp_name,$file);
						$file=str_replace($_SERVER['DOCUMENT_ROOT'],'',$file);
						return $file;
					}
				}
			}
			return false;
		}
	    //分页
		function page($allnum,$pernum,$showpage,$pageurl,$curpage)
		{
			$allpage=ceil($allnum/$pernum);
			if($curpage<0||$curpage>$allpage||$allnum==0||$showpage<0)
			{
				return '';
			}
			$pagehtml='';
			$pagehtmlcenter='';
			$pagehtmltop='<ul class="pagination">';
			if($curpage==1)
			{
				$pagehtmltop.='<li class="disabled"><a aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
			}
			else
			{
				$pre=$curpage-1;
				$pagehtmltop.='<li><a href="'.$pageurl.$pre.'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
			}
			if($curpage==$allpage)
			{
				$pagehtmlbottom='<li class="disabled"><a aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
			}
			else
			{
				$next=$curpage+1;
				$pagehtmlbottom='<li><a href="'.$pageurl.$next.'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
			}
			$pagehtmlbottom.='</ul>';
			if($showpage>$allpage)
			{
				for($i=1;$i<=$allpage;$i++)
				{
					if($i==$curpage)
					{
						$pagehtmlcenter.='<li class="active"><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
					}
					else
					{
						$pagehtmlcenter.='<li><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
					}
				}
			}
			else
			{
				$mid=floor($showpage/2);
				if($curpage-$mid>=1&&$curpage+$mid<=$allpage)
				{
					for($i=$curpage-$mid;$i<=$curpage+$mid;$i++)
					{
						if($i==$curpage)
						{
							$pagehtmlcenter.='<li class="active"><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
						}
						else
						{
							$pagehtmlcenter.='<li><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
						}
					}
				}
				else
				{
					if($curpage<=$mid)
					{
						for($i=1;$i<=$showpage;$i++)
						{
							if($i==$curpage)
							{
								$pagehtmlcenter.='<li class="active"><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
							}
							else
							{
								$pagehtmlcenter.='<li><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
							}
						}
					}
					if($curpage>$allpage-$mid)
					{
						for($i=$allpage-$showpage+1;$i<=$allpage;$i++)
						{
							if($i==$curpage)
							{
								$pagehtmlcenter.='<li class="active"><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
							}
							else
							{
								$pagehtmlcenter.='<li><a href="'.$pageurl.$i.'">'.$i.'</a></li>';
							}
						}
					}
				}
			}
			$pagehtml=$pagehtmltop.$pagehtmlcenter.$pagehtmlbottom;
			return $pagehtml;
		}
		//$params="{user:\"admin\",pwd:\"admin\"}";  
		//$headers=array('Content-type: text/json',"id: $ID","key:$Key");  
		//发送http请求
		function http_request($URL,$type,$params,$headers){  
			$ch=curl_init();  
			$timeout=5;  
			curl_setopt($ch,CURLOPT_URL,$URL); 
			if($headers!="")
			{  
				curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);  
			}
			else 
			{  
				curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-Type:application/json'));  
			}  
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
			curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);  
			switch($type)
			{  
				case "GET": 
					curl_setopt($ch,CURLOPT_HTTPGET,true);
					break;  
				case "POST": 
					curl_setopt($ch,CURLOPT_POST,true);   
					curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
					break;  
				case "PUT": 
					curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"PUT");   
					curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
					break;  
				case "DELETE":
					curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "DELETE");   
					curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
					break;  
			}  
			$file_contents=curl_exec($ch);
			return $file_contents;  
			curl_close($ch);  
		}
	}
	
?>