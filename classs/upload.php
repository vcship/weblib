<?php
	class upload 
	{ 
		public $path="./uploads";          //上传文件保存的路径
		private $allowtype=array('jpg','gif','png'); //设置限制上传文件的类型
		private $maxsize=6000000;           //限制文件上传大小（字节）
		private $israndname=true;           //设置是否随机重命名文件， false不随机
		public $originName;              //源文件名
		private $tmpFileName;              //临时文件名
		public $fileType;               //文件类型(文件后缀)
		public $fileSize;               //文件大小
		public $newFileName;              //新文件名
		private $errorNum=0;             //错误号
		private $errorMess="";             //错误报告消息
		
		//为单个成员属性设置值 
		public function setOption($key,$val){
			$this->$key=$val;
		}
		//接收上传文件
		public function uploadFile($fileField){
			if(!$this->checkFilePath()){       
				$this->errorMess=$this->getError();
				return false;
			}
			$name=$_FILES[$fileField]['name'];
			$tmp_name=$_FILES[$fileField]['tmp_name'];
			$size=$_FILES[$fileField]['size'];
			$error=$_FILES[$fileField]['error'];
			if(is_Array($name)){    
				$errors=array();
				for($i=0;$i<count($name);$i++){ 
					if($this->setFiles($name[$i],$tmp_name[$i],$size[$i],$error[$i])){
						if(!$this->checkFileSize()||!$this->checkFileType()){
							$errors[]=$this->getError();
							$return=false; 
						}
					}
					else{
						$errors[]=$this->getError();
						$return=false;
					}
				}
				if(!$return){
					$this->setFiles();
				}        	
				if($return){
					$fileNames=array();      
					for($i=0;$i<count($name);$i++){ 
						if($this->setFiles($name[$i],$tmp_name[$i],$size[$i],$error[$i])){
							$this->setNewFileName(); 
							if(!$this->copyFile()){
								$errors[]=$this->getError();
								$return=false;
							}
							$fileNames[]=$this->newFileName;  
						}          
					}
					$this->newFileName=$fileNames;
				}
				$this->errorMess=$errors;
				return $return;
			} 
			else{
				if($this->setFiles($name,$tmp_name,$size,$error)){
					if($this->checkFileSize()&&$this->checkFileType()){ 
						$this->setNewFileName(); 
						if($this->copyFile()){ 
							return true;
						}
						else{
							$return=false;
						}
					}
					else{
						$return=false;
					}
				} 
				else{
					$return=false; 
				}
				if(!$return){
					$this->errorMess=$this->getError(); 
				}					
				return $return;
			}
		}

		//获取上传后的文件名称 
		public function getFileName(){
			return $this->newFileName;
		}
	  
		//上传失败后，调用该方法则返回，上传出错信息 
		public function getErrorMsg(){
			return $this->errorMess;
		}
	  
		//设置上传出错信息
		private function getError() {
			$str="上传文件<font color='red'>{$this->originName}</font>时出错 :";
			switch($this->errorNum){
				case 4:$str.="没有文件被上传"; break;
				case 3:$str.="文件只有部分被上传"; break;
				case 2:$str.="上传文件的大小超过了HTML表单中MAX_FILE_SIZE选项指定的值"; break;
				case 1:$str.="上传的文件超过了php.ini中upload_max_filesize选项限制的值"; break;
				case -1:$str.="未允许类型"; break;
				case -2:$str.="文件过大,上传的文件不能超过{$this->maxsize}个字节"; break;
				case -3:$str.="上传失败"; break;
				case -4:$str.="建立存放上传文件目录失败，请重新指定上传目录"; break;
				case -5:$str.="必须指定上传文件的路径"; break;
				default: $str.="未知错误";
			}
			return $str.'<br>';
		}
	  
		//设置和$_FILES有关的内容
		private function setFiles($name="",$tmp_name="",$size=0,$error=0){
			$this->setOption('errorNum',$error);
			if($error){
				return false;
			}	
			$this->setOption('originName', $name);
			$this->setOption('tmpFileName',$tmp_name);
			$aryStr=explode(".",$name);
			$this->setOption('fileType',strtolower($aryStr[count($aryStr)-1]));
			$this->setOption('fileSize',$size);
			return true;
		}
	  
	  
		//设置上传后的文件名称
		private function setNewFileName(){
			if($this->israndname){
				$this->setOption('newFileName',$this->proRandName());  
			} 
			else{ 
				$this->setOption('newFileName',$this->originName);
			} 
		}
	  
		//检查上传的文件是否是合法的类型
		private function checkFileType(){
			if(in_array(strtolower($this->fileType),$this->allowtype)){
				return true;
			}
			else{
				$this->setOption('errorNum',-1);
				return false;
			}
		}
	  
		//检查上传的文件是否是允许的大小 
		private function checkFileSize() {
			if($this->fileSize>$this->maxsize){
				$this->setOption('errorNum', -2);
				return false;
			}
			else{
				return true;
			}
		}
	  
		//检查是否有存放上传文件的目录 
		private function checkFilePath() {
			if(empty($this->path)){
				$this->setOption('errorNum',-5);
				return false;
			}
			if(!file_exists($this->path)||!is_writable($this->path)){
				if(!@mkdir($this->path,0755)){
					$this->setOption('errorNum', -4);
					return false;
				}
			}
		  return true;
		}
	  
		//设置随机文件名
		private function proRandName(){    
			$fileName=date('YmdHis')."_".rand(100,999);    
			return $fileName.'.'.$this->fileType; 
		}
	  
		//复制上传文件到指定的位置 
		private function copyFile(){
			if(!$this->errorNum){
				$path=rtrim($this->path,'/').'/';
				$path.=$this->newFileName;
				if(@move_uploaded_file($this->tmpFileName,$path)) {
					return true;
				}
				else{
					$this->setOption('errorNum',-3);
					return false;
				}
			} 
			else{
				return false;
			}
		}
	}
?>