<?php
if(!defined('CORE_DIR')||!defined('CONTROLLERS_DIR')||!defined('VIEWS_DIR')||
!defined('MODELS_DIR')||!defined('CONFIG_DIR')||!defined('CLASSS_DIR')||!defined('LOGS_DIR')||
!defined('STATIC_DIR')||!defined('UPLOAD_DIR')||!defined('CONFIG_FILE')||!defined('OTHERS_DIR'))
{
	status(500);
	return;
}	
$endrun=false;
$debug=false;
$vars=null;
$charset='UTF-8';
//加载配置文件
if(file_exists(CONFIG_DIR.CONFIG_FILE))
{
	require_once(CONFIG_DIR.CONFIG_FILE);
}
//检查日志目录
function checklogdir()
{
	if(!file_exists(LOGS_DIR))
	{
		@mkdir(LOGS_DIR,'0755');
	}
}
//获取配置
function get_config($key)
{
	if(isset($GLOBALS['config']))
	{
		if(is_array($GLOBALS['config']))
		{
			if(array_key_exists($key,$GLOBALS['config']))
			{
				return $GLOBALS['config'][$key];
			}
		}
	}
	return null;
}
//初始化
function inti()
{
	ini_set("display_errors","on");
	error_reporting(0);
	if(version_compare(PHP_VERSION,'5.4.0','<')) 
	{
		ini_set('magic_quotes_runtime',0);
	}
	if(get_config('session')=='on')
	{
		session_start();
	}
	if(get_config('charset')!=null)
	{
		ini_set('default_charset',get_config('charset'));
		$GLOBALS['charset']=get_config('charset');
	}
	if(get_config('appname')!=null)
	{
		header('X-Powered-By:'.get_config('appname'));
	}
	if(get_config('timezone')!=null)
	{
		ini_set('date.timezone',get_config('timezone'));
	}
	if(get_config('debug')!=null)
	{
		if(get_config('debug')==true)
		{
			$GLOBALS['debug']=true;
		}
	}
	register_shutdown_function('shutdown_func');
	set_error_handler("myErrorHandler");
	set_exception_handler("myException");
	checklogdir();
}
//生成令牌
function token($name,$str)
{
	$time=time();
	$str=md5($str.$time);
	session($name,$str.'_'.$time);
}
//检测令牌
function istoken($name,$str,$expire)
{
	$token=session($name);
	if($token!==false)
	{
		if(strpos($token,'_')!==false)
		{
			$a=explode('_',$token);
			$time=intval($a[1]);
			if($a[0]==md5($str.$time))
			{
				$now=time();
				if($now-$time<$expire){
					return true;
				}
			}
		}
	}
	return false;
}

//判断请求方法
function is_method($method)
{
	$m=$_SERVER['REQUEST_METHOD'];
	if(strtoupper($method)==$m)
	{
		return true;
	}
	return false;
}
//判断是否是ajax
function is_ajax()
{
	if(isset($_SERVER['HTTP_X_REQUESTED_WITH'])&&strtolower($_SERVER['HTTP_X_REQUESTED_WITH'])=='xmlhttprequest')
    {
        return true;
    }
    else
    {
        return false;
    }
}
//获取根目录url
function baseurl($type)
{
	if((isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on')||(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])&&$_SERVER['HTTP_X_FORWARDED_PROTO']=='https'))
	{
		$protocol='https://';
	}
	else
	{
		$protocol='http://';
	}
	if($type=='short')
	{
		return $protocol.$_SERVER['HTTP_HOST'];
	}
	else
	{
		return dirname($protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	}
}
//设置session
function session($name,$value=null)
{
	if($value!=null)
	{
		$_SESSION[$name]=$value;
		return $_SESSION[$name];
	}
	else
	{
		if(isset($_SESSION[$name]))
		{
			return $_SESSION[$name];
		}
		else
		{
			return false;
		}
	}
}
//删除session
function unsession($name)
{
	if(isset($_SESSION[$name]))
	{
		unset($_SESSION[$name]);
	}
}
//设置cookie
function cookie($name,$value=null,$expire=null,$path=null,$domain=null)
{
	if($value!=null)
	{
		setcookie($name,$value);
		if($expire!=null)
		{
			setcookie($name,$value,time()+$expire);
			if($path!=null)
			{
				setcookie($name,$value,time()+$expire,$path);
				if($domain!=null)
				{
					setcookie($name,$value,time()+$expire,$path,$domain);
				}
			}
		}
		return $_COOKIE[$name];
	}
	else
	{
		if(isset($_COOKIE[$name]))
		{
			return $_COOKIE[$name];
		}
		else
		{
			return false;
		}
	}
}
//删除cookie
function uncookie($name)
{
	if(isset($_COOKIE[$name]))
	{
		setcookie($name,"",time()-3600);
	}
}
//获取post参数 
function post($name,$type='str')
{
	if(isset($_POST[$name]))
	{
		if($type=='str')
		{
			return $_POST[$name];
		}
		if($type=="int")
		{
			if($_POST[$name]===intval($_POST[$name]).'')
			{
				return intval($_POST[$name]);
			}
		}
		if($type=='flo')
		{
			if($_POST[$name]===floatval($_POST[$name]).'')
			{
				return floatval($_POST[$name]);
			}
		}
	}
	return false;
}
//日志
function writelog($text,$type)
{
	$timelong=date('Y-m-d H:i:s',time());
	$timeshort=date('Y-m-d',time());
	$filename=LOGS_DIR.$timeshort.'.txt';
	$file=fopen($filename,"a");
	$text="[".$type."]  ".$text.'  '.$timelong."\n";
	fwrite($file,$text);
	fclose($file);
}
//获取get参数
function get($name,$type='str')
{
	if(isset($_GET[$name]))
	{
		if($type=='str')
		{
			return $_GET[$name];
		}
		if($type=="int")
		{
			if($_GET[$name]===intval($_GET[$name]).'')
			{
				return intval($_GET[$name]);
			}
		}
		if($type=='flo')
		{
			if($_GET[$name]===floatval($_GET[$name]).'')
			{
				return floatval($_GET[$name]);
			}
		}
	}
	return false;
}
//页面跳转
function redirect($url)
{
	header('Location:'.$url);
	return;
}
//载入model
function model($name)
{
	$name.='_model';
	if(file_exists(MODELS_DIR.$name.'.php'))
	{
		require_once(MODELS_DIR.$name.'.php');
		if(class_exists($name))
		{
			$c=new $name;
			return $c;
		}
	}
	return false;
}
//载入class
function load_class($name,$type=null)
{
	if($type==null)
	{
		if(file_exists(CLASSS_DIR.$name.'.php'))
		{
			require_once(CLASSS_DIR.$name.'.php');
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(file_exists(CLASSS_DIR.$name.'.php'))
		{
			require_once(CLASSS_DIR.$name.'.php');
			if(class_exists($name))
			{
				$c=new $name;
				return $c;
			}
			return false;
		}
	}
}
//脚本结束执行函数
function shutdown_func()
{
	if($error=error_get_last())
	{
		$text='错误信息：'.$error['message'].'  错误文件：'.$error['file'].'  错误行:'.$error['line'];
		if($GLOBALS['debug'])
		{
			echo $text."\n";
			writelog($text,'error');
		}
		else
		{
			writelog($text,'error');
		}
	} 
	if(!$GLOBALS['endrun'])
	{
		status(500);
	}
}
//用户定义的异常处理函数
function myException($exception) 
{
	$text=$exception->getMessage();
	if($GLOBALS['debug'])
	{
		echo $text."\n";
		writelog($text,'exception');
	}
	else
	{
		writelog($text,'exception');
	}
 }
//自定义错误处理函数
function myErrorHandler($errno,$errstr,$errfile,$errline)
{
	$text='错误信息：'.$errstr.'  错误文件：'.$errfile.'  错误行:'.$errline;
	if($GLOBALS['debug'])
	{
		echo $text."\n";
		writelog($text,'error');
	}
	else
	{
		writelog($text,'error');
	}
}
//构建url
function url($c,$m,$query=null)
{
	if(get_config('pathinfo')==1)
	{
		if(is_array($query))
		{
			return '/'.$c.'/'.$m.'?'.http_build_query($query);
		}
		else{
			return '/'.$c.'/'.$m;
		}
	}
	else if(get_config('pathinfo')==2)
	{
		if(is_array($query))
		{
			return $_SERVER['PHP_SELF'].'?c='.$c.'&m='.$m.'&'.http_build_query($query);
		}
		else{
			return $_SERVER['PHP_SELF'].'?c='.$c.'&m='.$m;
		}
	}
	else
	{
		if(is_array($query))
		{
			return $_SERVER['PHP_SELF'].'?c='.$c.'&m='.$m.'&'.http_build_query($query);
		}
		else
		{
			return $_SERVER['PHP_SELF'].'?c='.$c.'&m='.$m;
		}
	}
}
//处理路由  url重写
function map_rewrite()
{
	$request_url=$_SERVER['REQUEST_URI'];
	if($request_url=='/')
	{
		instance('index','index');
	}
	else
	{
		$a=explode('/',$request_url);
		$length=count($a);
		$c=$a[1];
		if($length>=3)
		{
			$m=$a[2];
			if($m=="")
			{
				instance($c,$c);
			}
			else
			{
				instance($c,$m);
			}
		}
		else
		{
			instance($c,$c);
		}
	}
	$GLOBALS['endrun']=true;
}
//处理路由  普通模式
function map_general()
{
	$c=get("c","str");
	$m=get("m","str");
	if($c!==false&&$m!=false)
	{
		instance($c,$m);
	}
	else
	{
		if($c!==false&&$m===false)
		{
			instance($c,$c);
		}
		else
		{
			instance('index','index');
		}
	}
	$GLOBALS['endrun']=true;
}
//实例化方法
function instance($c,$m)
{
	$c.='_controller';
	$m.='_action';
	if(file_exists(CONTROLLERS_DIR.$c.'.php'))
	{
		require_once(CONTROLLERS_DIR.$c.'.php');
		if(class_exists($c))
		{
			if(method_exists($c,$m))
			{
				$r=new $c;
				$res=true;
				if(method_exists($c,'_before'))
				{
					$res=$r->_before();
				}
				if($res)
				{
					$r->$m();
				}
				if(method_exists($c,'_after'))
				{
					$r->_after();
				}
			}
			else
			{
				status(404);
			}
		}
		else
		{
			status(404);
		}
	}
	else
	{
		status(404);
	}
}
//返回状态码
function status($code)
{
	if($code==404)
	{
		if(get_config('errorview')==null||get_config('errorview')=='')
		{
			header('HTTP/1.1 404 Not Found');
			text('404 错误！'."\n");
			return;
		}
		else
		{
			header('HTTP/1.1 404 Not Found');
			render($view,array('code'=>404));
		}
	}
	if($code==500)
	{
		if(get_config('errorview')==null||get_config('errorview')=='')
		{
			header('HTTP/1.1 500 Internal Server Error');
			text('500 错误！'."\n");
			return;
		}
		else
		{
			header('HTTP/1.1 500 Internal Server Error');
			render($view,array('code'=>500));
		}
	}
}
//返回json
function json($arr)
{
	header('content-type:application/json; charset='.$GLOBALS['charset']);
	if(is_array($arr))
	{
		echo json_encode($arr);
		return;
	}
	echo json_decode(array('code'=>0,'message'=>'parameter error'));
}
//返回json信息
function message($code,$str,$data=null)
{
	$a=array("code"=>$code,"message"=>$str,"data"=>$data);
	json($a);
}
//返回文本
function text($str)
{
	header('content-type:text/plain; charset='.$GLOBALS['charset']);
	echo $str;
}
//加载视图
function render($view,$data=null)
{
	$path=VIEWS_DIR.$view.'_view.php';
	if(file_exists($path))
	{
		$GLOBALS['vars']=$data;
		header('content-type:text/html; charset='.$GLOBALS['charset']);
		ob_start();
		if(is_array($data))
		{
			extract($data);
		}
		require_once($path);
		echo trim(ob_get_clean());
	}
	else
	{
		text('找不到视图文件！');
	}
}
//加载模板
function templete($view){
	$path=VIEWS_DIR.$view.'_view.php';
	if(file_exists($path))
	{
		$data=$GLOBALS['vars'];
		if(is_array($data))
		{
			extract($data);
		}
		require_once($path);
	}
	else
	{
		echo('找不到模板！');
	}
}
//运行 pathinfo=1 需要根目录+url rewrite  pathinfo=2 适用大多数场景
function run()
{
	inti();
	if(get_config('pathinfo')==1)
	{
		map_rewrite();
	}
	else if(get_config('pathinfo')==2)
	{
		map_general();
	}
	else
	{
		map_general();
	}
}
run();
?>